# Android

The latest revision to the android system is using the android emulator.

The android emulator must be accessible on the host system, using the `https://gitlab.com/mergetb/droid-emu` github repo to install the prerequisites.  After the prerequisites are installed, make sure to set the appropriate environmental variables.


### Required Environment Variables

`CMDR_IP` : IP address of the commander (default: localhost)

`CMDR_PORT` : Port for the commander service (default: 6000)

`AVD_PORT` : Port where the android emulator is being run (default: 15001)

`AVD_IP` : IP  where the android emulator is being run (default: `$(ifconfig eno1 | egrep -o "inet addr:(([0-9]{1,3}\.){3}[0-9]{1,3})" | cut -d ":" -f 2)`)

`DRIVER_IFACE` or `DRIVER_IP` : IP to use to give to commander for the driver, default for IFACE is `eno1`, no current default for IP, although IP will override IFACE settings.

`DROID_USER_HOME` : Path to /home/user who installed droid-emu

`DROID_EMU_PATH` : Path to the droid-emu directory installed on the system

These variables live in `.env` which is used by docker-compose as runtime environmental variables.  These variables are used for `avd_host_manager.py` and `drivers/drivers.py`.  The `CMDR` variables are used by the drivers, while `AVD_PORT` and `HOST_IP` are used by the avd/emulator code.


### How to run

To run the code, two services must be running.  The _first_ is to run the `avd_host_manager.py` code as root.  This code is responsible for interacting with the host system and controlling both the avd creation/deletion, as well as the android emulator status.  AVD images controlled by this code are stored in `/Android/` and the confing file is symlinked into the directory of the user where the droid-emu repository was installed.

The _second_ piece of code necessary to be run is the docker container responsible for running the drivers code that implements the state machine for the Android devices.

process 1: # `source sourceme.sh && python avd_host_manager.py`

process 2: # `source sourceme.sh && docker-compose -f docker-compose.yml up`


#### Testing

If the driver is being tested - setting the test variables to True will prepare a fake uuid for testing.

drivers/drivers.py: `run_device_server(testing=True, client_testing=True)`

To run tests to validate the driver model, first start with modifying the commander code to enable testing.  Doing so changes the commander code from being a service to being a client, sending commands to the driver service.

commander/main.go: `client_testing := true`

Step 1.  Start avd manager

Step 2.  Start docker-compose (drivers)

Step 3.  Run main.go (go build main.go && sudo ./main)

Sudo is required for the commander as the commander log files live in /var/log/tb.


#### Sample AVD/Emulator Testing

Note: That by default the droid-emu installation does not install android-P

Create new AVD Image:
    ```
    curl -H "Content-Type: application/json" -X POST -d '{"abi":"x86_64", "device":"pixel", "sdk_api":"P", "name":"swap"}' http://localhost:15001/avd/create
    ```

Delete AVD Image:
    ```
    curl -H "Content-Type: application/json" -X POST -d '{"name":"swap"}' http://localhost:15001/avd/delete
    ```

Start Emulator:
    ```
    curl -H "Content-Type: application/json" -X POST -d '{"name":"swap"}' http://localhost:15001/emulator/start
    ```

Stop Emulator:
    ```
    curl -H "Content-Type: application/json" -X POST -d '{"name":"swap"}' http://localhost:15001/emulator/kill
    ```


All endpoints: 
```
@app.route('/avd/create', methods=['POST'])
@app.route('/avd/check', methods=['POST'])
@app.route('/avd/delete', methods=['POST'])
@app.route('/emulator/kill', methods=['POST'])
@app.route('/emulator/ps', methods=['POST'])
@app.route('/emulator/serial', methods=['GET'])
@app.route('/emulator/start', methods=['POST'])
@app.route('/emulator/tag', methods=['POST'])
@app.route('/emulator/reset', methods=['POST'])
@app.route('/emulator/restart', methods=['POST']) 
@app.route('/')
```
