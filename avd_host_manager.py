# this code is required for running on the host system
# it is responsible for creating global android virtual devices

import logging  # log data
from sys import exit # exit
import os  # check if file exists and environment variables
from subprocess import PIPE, Popen  # handle all communication with android
from json import dumps, loads  # converting to json, and back to string
import time  # hack to sleep after starting emulator

from flask import Flask, request  # api service

# pylint: disable=fixme, invalid-name
app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)

# i am sure there is a 100% better way of implementing this
USER_PATH = os.environ['DROID_USER_HOME']
TOOL_PATH = "{}/tools".format(os.environ['DROID_EMU_PATH'])
AVD_PATH = "{}/.android/avd".format(USER_PATH)
AVDSTORED = "/Android"


# TODO: this fails because of nohup appending - hard to determine what
# errored - classic problem of distributed logging
def error_in_logs():
    cmd = 'sudo tail -n 3 nohup.out'
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, _ = proc.communicate()
    if 'PANIC' not in out.decode('utf-8'):
        return False
    return out.decode('utf-8')


@app.route('/avd/delete', methods=['POST'])
def delete_avd(name=None):
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
        if not name:
            name = input_json.get('name', None)
    if not name:
        return dumps({"stdout": "", "stderr": "avd name missing"})
    cmd = '%s/bin/avdmanager delete avd -n %s' % (TOOL_PATH, name)
    LOGGER.debug(cmd)
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = proc.communicate()
    # check if the .ini file is still in AVD_PATH, if it is, delete it
    if os.path.isfile("{}/{}.ini".format(AVD_PATH, name)):
        os.remove("{}/{}.ini".format(AVD_PATH, name))
    return dumps({"stdout": out.decode('utf=8'), "stderr": err.decode('utf-8')})


# TODO: it is possible for the file /Android/name to exist without avd knowing
# this will cause weird behavior and require rm -rf /Android/name*
@app.route('/avd/create', methods=['POST'])
def create_avd(device=None, sdcard=None, api_version=None, abi=None, name=None):
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
    # get this information from the user.
    # physical device ~ 'nexus 7' 'pixel'

    # if device is not set, binary, either all or set, or all will get set
    if not device:
        device = input_json.get('device', None)
        # path to the sdcard image
        sdcard = input_json.get('sdcard', None)
        # tag, which system-image to use
        # api to use 26, 27, etc
        api_version = input_json.get('sdk_api', None)
        # architecture for emulating: x86_64, armv7, etc
        abi = input_json.get('abi', None)

        # TODO: take this functionality out of the control of the user
        name = input_json.get('name', None)

    if sdcard and not os.path.isfile(sdcard):
        sdcard = None

    # set path where to store this avd
    droid_loc = "{stored}/{name}".format(stored=AVDSTORED, name=name)
    cmd = "{tools}/bin/avdmanager -v create avd -n {name} "\
        "-b google_apis/{abi} -k 'system-images;android-{sdk_api};google_apis;{abi}' "\
        "-d {device} {sdcard} {dpath}".format(
            tools=TOOL_PATH,
            name=name,
            abi=abi,
            sdk_api=api_version,
            device=device,
            sdcard="-c " + sdcard if sdcard else "",
            dpath="-p " + droid_loc if droid_loc else "",
        )
    LOGGER.debug(cmd)
    # the issue at this point is that this command cannot execute in the
    # container, avd shell script requires java and sdk tools.
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = proc.communicate()
    # if there wasnt a problem creating an avd, we need to symlink it
    if not err:
        cmd = "ln -s {home}/avd/{name}.ini {avd_path}".format(
            home="/root/.android",  # TODO: find a way to link to home when sudo -s
            name=name,
            avd_path=AVD_PATH,
        )
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        # serialize - make sure we finish linked before we exit
        _, _ = proc.communicate()
    # error occured during create
    return dumps({"stdout": out.decode('utf=8'), "stderr": err.decode('utf-8')})


# find the process for the emulator given we know the avd name
@app.route('/emulator/ps', methods=['POST'])
def find_process_num(avd_name=None):
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
    if not avd_name:
        avd_name = input_json.get('name', None)
    LOGGER.debug('check ps: avd_name: %s', avd_name)
    if not avd_name:
        return ''
    # look through process table, find entry with -avd <name>
    # ignore the current grep, and the child spawned calling qemu - we want to kill parent
    # pylint: disable=anomalous-backslash-in-string
    # backslash needs to be there in grep
    cmd = 'ps -aux | grep "\-avd {}" | grep -v "/qemu/" | grep -v "grep" '\
        '| xargs | cut -d " " -f 2'.format(avd_name)
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = proc.communicate()
    if err:
        LOGGER.error(str(err))
        return ''
    LOGGER.debug("process for emulator: %s", out.decode('utf-8').strip())
    # if the thing we return cant be turned to an int, we didnt find a process.
    try:
        int(out.decode('utf-8').strip())
    except ValueError:
        return ''
    # we found a process, return the string version
    return out.decode('utf-8').strip()


# kill the emulator, this is necessary if we use setup to reconfigure avd
@app.route('/emulator/kill', methods=['POST'])
def kill_emulator(name=None, uuid=None):
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
        if not name and not uuid:
            name = input_json.get('name', None)
            uuid = input_json.get('uuid', None)
    LOGGER.info(name)
    if not name:
        # get the keys (serial names) for each device
        devices = [list(x.keys())[0] for x in get_devices()]
        for droid in devices:
            res = run_shell_command("cat /system/uuid", droid)
            LOGGER.debug(res)
            if res['stdout']:
                if res['stdout'].strip() == uuid:
                    res = run_shell_command("cat /system/name", droid)
                    LOGGER.debug(res)
                    if res['stdout']:
                        name = res['stdout'].strip()
                        break
    LOGGER.info('killing: %s', name)
    if not name:
        return dumps({"stdout": "", "stderr": "unable to match uuid to avd name"})
    cmd = "kill {}".format(find_process_num(name))
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = proc.communicate()
    if err:
        LOGGER.error(str(err))
    return dumps({"stdout": out.decode('utf=8').strip(), "stderr": err.decode('utf-8').strip()})


# TODO: find the next available vnc port - this is a hack.
# will break with other vnc applications running or ports above 5900
def find_next_vnc_port():
    # look through process table, find entry with -avd <name>
    # ignore the current grep, and the child spawned calling qemu - we want to kill parent
    # pylint: disable=anomalous-backslash-in-string
    cmd = 'ps -aux | grep "\-vnc" | grep -v "grep" '\
        '| rev | cut -d " " -f 1,2'
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    out, err = proc.communicate()
    if err:
        LOGGER.error(str(err))
        return None
    output = out.decode('utf-8').split('\n')
    highest_port = 0
    # TODO: this is not smart, it will just keep going up until ragnarok
    for vnc_cmd in output:
        vnc_tuple = vnc_cmd.split(' ')
        if len(vnc_tuple) <= 1:
            continue
        if vnc_tuple[1] == 'cnv-':
            # in reversed scheme there is still the colon attached
            vnc_val = vnc_tuple[0][:-1]
            if int(vnc_val) > highest_port:
                highest_port = int(vnc_val)
    return highest_port + 1

# TODO: need an offline name to uuid mapping, because start will likely
# be used from a uuid perspective, but since the device is likely to be
# turned off, there will be no way to retrieve the name.


@app.route('/emulator/start', methods=['POST'])
def start_emulator(name=None):
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
        LOGGER.debug(input_json)
        if not name:
            name = input_json.get('name', None)
    if not name:
        return dumps({'stdout': '', 'stderr': 'no avd name provided'})
    LOGGER.debug("creating emulator: %s", name)
    vnc_port = find_next_vnc_port()
    # hoping i dont need to know output or error of this command
    cmd = 'sudo echo "" > nohup.out'
    _ = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    # FIXME: based on how we lookup vnc, that will need to be last argument
    cmd = 'nohup sudo {tools}/emulator -avd {name} '\
        '-datadir {stored}/{name} -writable-system -skin 1080x1920 -gpu off '\
        '-no-window -noaudio -qemu -vnc :{vnc_port} 2>&1 &'.format(
            tools=TOOL_PATH,
            name=name,
            stored=AVDSTORED,
            vnc_port=vnc_port,
        )
    LOGGER.debug(cmd)
    # start_new_session is suppose to be new way of daemoning/abandoning child proc
    _ = Popen(cmd, shell=True, start_new_session=True)

    # TODO, there may be a ps, but it wont help if the device hasnt booted.
    # timeout = 20
    # while find_process_num(avd_name=name) and timeout > 0:
    #     time.sleep(5)
    #     timeout -= 5
    time.sleep(15)

    # TODO: this is broken ~~~ if previous command panics and this is fine.
    error = error_in_logs()
    if error:
        return dumps({"stdout": '', "stderr": error})
    return dumps({"stdout": '', "stderr": ''})


# this will break if we change where they can store images not in AVDSTORED
@app.route('/avd/check', methods=['POST'])
def check_avd_exists():
    input_json = {}
    if request.is_json:
        input_json = request.get_json()
    avd_name = input_json.get('name', None)
    if avd_name:
        cmd = "ls {root}/{name}".format(root=AVDSTORED, name=avd_name)
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
        _, err = proc.communicate()
        # check if we got an error such that the file does not exist
        if err.decode('utf-8'):
            return dumps({"stdout": "", "stderr": "{} does not exist".format(avd_name)})
        # file exists
        return dumps({"stdout": "success", "stderr": ""})
    # did not supply a name, so lets error to help user know why
    return dumps({"stdout": "", "stderr": "key: [name] not supplied"})


# this is not thread safe, there can only be one unmarked device
# otherwise this will mix up devices - need a better solution
# for marking serial devices after device creation.
def find_unmarked():
    devices = [list(x.keys())[0] for x in get_devices()]
    for droid in devices:
        res = run_shell_command("cat /system/uuid", droid)
        LOGGER.debug(res)
        if res['stderr']:
            return droid
    return None


@app.route('/emulator/serial', methods=['GET'])
def find_serial(uuid=None):
    if not uuid:
        if request.is_json:
            input_json = request.get_json()
            LOGGER.debug(input_json)
            uuid = input_json.get('uuid', None)
        else:
            return ''
    LOGGER.debug("find_serial: uuid: %s", uuid)
    devices = [list(x.keys())[0] for x in get_devices()]
    for droid in devices:
        res = run_shell_command("cat /system/uuid", droid)
        if res['stdout'].strip() == uuid:
            return droid
    return ''


@app.route('/emulator/tag', methods=['POST'])
def export_tag():
    if request.is_json:
        input_json = request.get_json()
        LOGGER.debug(input_json)
    uuid = input_json.get('uuid', None)
    if not uuid:
        return dumps({'stdout': '', 'stderr': 'no uuid found'})
    name = input_json.get('name', None)
    if not name:
        return dumps({'stdout': '', 'stderr': 'no name found'})

    # on first boot lets try and find an unmarked
    if input_json.get('first_boot', None):
        serial = find_unmarked()
    # if serial still is null, maybe the avd is old, but new emulator
    if not serial:
        serial = find_serial(uuid)
    LOGGER.debug("serial: %s", serial)
    if not serial:
        return dumps({'stdout': '', 'stderr': 'no missing serial found'})
    tagged = tag_droid(name=name, uuid=uuid, serial=serial)
    LOGGER.debug('tagged: %s', tagged)
    if tagged['stderr']:
        return dumps({'stdout': '', 'stderr': tagged['stderr']})
    time.sleep(5)
    return dumps({'stdout': 'success', 'stderr': ''})


# pylint: disable=too-many-return-statements
@app.route('/emulator/reset', methods=['POST'])
def clobber_emulator():
    input_json = request.get_json()
    name = input_json.get('name', None)
    uuid = input_json.get('uuid', None)
    device = input_json.get('device', None)
    sdcard = input_json.get('sdcard', None)
    api_version = input_json.get('sdk_api', None)
    abi = input_json.get('abi', None)
    # because name=uuid here, it doesnt have to but, if it is.

    serial = find_serial(uuid)
    LOGGER.debug("serial: %s", serial)

    # get the old device name
    old_avd_name = run_shell_command('cat /system/name', serial)['stdout'].strip()
    LOGGER.info('old avd name: %s', old_avd_name)

    # stop old emulator
    killed = loads(kill_emulator(name=uuid, uuid=uuid))
    LOGGER.info('killed: %s', killed)
    if killed['stderr']:
        return dumps({'stdout': 'killed', 'stderr': killed['stderr']})

    # takes time to clean up process
    time.sleep(5)

    # delete old avd
    deleting = loads(delete_avd(old_avd_name))
    LOGGER.info('deleted avd: %s', deleting)
    if deleting['stderr']:
        return dumps(deleting)

    # create new avd
    created = loads(
        create_avd(
            device=device, sdcard=sdcard, api_version=api_version, abi=abi, name=name
        )
    )
    LOGGER.info('created: %s', created)
    if created['stderr']:
        return dumps({'stdout': 'created', 'stderr': created['stderr']})

    # start new emulator
    started = loads(start_emulator(name=name))
    LOGGER.info('started: %s', started)
    if started['stderr']:
        return dumps({'stdout': 'started', 'stderr': started['stderr']})

    # same as start, need a way to wait for device to come back up
    time.sleep(15)

    # tag the new emulator device
    serial = find_unmarked()
    if not serial:
        return dumps({'stdout': '', 'stderr': 'no missing serial found'})
    LOGGER.debug('unmarked serial: %s', serial)
    tagged = tag_droid(name=name, uuid=uuid, serial=serial)
    LOGGER.info('tagged: %s', tagged)
    if tagged['stderr']:
        return dumps({'stdout': 'tagged', 'stderr': tagged['stderr']})

    return dumps({"stdout": 'success', "stderr": ''})


@app.route('/')
def web_root():
    return 'whoops!'


def check_sudo():
    LOGGER.info(os.geteuid())
    return os.geteuid() == 0


def get_devices(model: bool = False, device: bool = False,
                product: bool = False, transport: bool = False):
    d_proc = Popen(['adb', 'devices', '-l'], stdout=PIPE, stderr=PIPE)
    out, _ = d_proc.communicate()
    device_info = out.decode('utf-8').strip().split('\n')[1:]
    device_list = []
    LOGGER.debug("raw output for devices -l:\n %s", out.decode('utf-8').strip())
    # TODO: if this errors, we should try killing the server and running 4-times
    for dev in device_info:
        line = dev.split(' ')
        devices = {}
        devices[line[0]] = {}
        # this will break when adb craps out and gives info about offline emulators
        for k_v in line[1:]:
            if k_v and k_v != 'device':
                # this is when there are no correct devices, no : to split on
                try:
                    key, value = k_v.split(':')
                except ValueError:
                    return []
                if model and key == 'model':
                    devices[line[0]].update(model=value)
                if device and key == 'device':
                    devices[line[0]].update(device=value)
                if product and key == 'product':
                    devices[line[0]].update(product=value)
                if transport and key == 'transport':
                    devices[line[0]].update(transport=value)
        device_list.append(devices)

    # python 3 keys is a non-list type
    LOGGER.debug("set of devices being returned: %s", str(device_list))
    return device_list


def run_shell_command(shell_cmd, serial):
    shell_proc = Popen(
        ['adb', '-s', serial, 'shell', shell_cmd],
        stdout=PIPE, stderr=PIPE
    )
    out, err = shell_proc.communicate()
    LOGGER.debug(
        "run_shell: %s -> (%s, %s)", shell_cmd,
        out.decode().strip(), err.decode().strip()
    )
    return {"stdout": out.decode('utf-8'), "stderr": err.decode('utf-8')}


# need to be in adb root mode before calling this function
def adb_remount(serial=None) -> bool:
    if serial:
        start = Popen(['adb', '-s', serial, 'remount'], stdout=PIPE, stderr=PIPE)
    else:
        start = Popen(['adb', 'remount'], stdout=PIPE, stderr=PIPE)
    # blocking, wait to finish
    out, err = start.communicate()
    LOGGER.debug("adb remount:\n stdout: %s\n stderr: %s\n", out, err)
    if 'succeed' in out.decode('utf-8').strip():
        return True
    return False


def start_adb_root(serial=None) -> bool:
    LOGGER.info('serial: %s', serial)
    if serial:
        start = Popen(
            ['adb', '-s', serial, 'root'],
            stdout=PIPE, stderr=PIPE
        )
    else:
        start = Popen(['adb', 'root'], stdout=PIPE, stderr=PIPE)
    # blocking, wait to finish
    out, err = start.communicate()
    LOGGER.debug("adb root:\n stdout: %s\n stderr: %s\n", out, err)
    if err:
        return False
    LOGGER.info("started adb server as root")
    return True


def start_adb_unroot(serial=None) -> bool:
    if serial:
        start = Popen(['adb', '-s', serial, 'unroot'], stdout=PIPE, stderr=PIPE)
    else:
        start = Popen(['adb', 'unroot'], stdout=PIPE, stderr=PIPE)
    # blocking, wait to finish
    out, err = start.communicate()
    LOGGER.debug("adb unroot:\n stdout: %s\n stderr: %s\n", out, err)
    if err:
        return False
    LOGGER.info("started adb server as normal user")
    return True


def write_to_system(place, content, serial):
    ret = run_shell_command("echo %s > /system/%s" % (content, place), serial)
    LOGGER.info(ret)
    return ret


def tag_droid(name=None, uuid=None, serial=None):
    if request.is_json:
        input_json = request.get_json()
        if not name and not uuid and not serial:
            name = input_json.get('name', None)
            uuid = input_json.get('uuid', None)
            serial = input_json.get('serial', None)
    LOGGER.debug("tagging: %s", input_json)
    # root
    _ = start_adb_root(serial=serial)

    # remount
    remount = adb_remount(serial)
    if not remount:
        return {"stdout": '', "stderr": 'adb remount failed'}
    # write to /system/uuid
    write_to_system('uuid', uuid, serial)
    # write to /system/name
    write_to_system('name', name, serial)

    # unroot
    _ = start_adb_unroot(serial=serial)

    return {"stdout": 'success', "stderr": ''}


@app.route('/emulator/restart', methods=['POST'])
def restart_emulator():
    input_json = request.get_json()
    uuid = input_json.get('uuid', None)
    serial = find_serial(uuid)
    avd_name = run_shell_command('cat /system/name', serial)['stdout'].strip()
    LOGGER.info('avd name: %s', avd_name)

    LOGGER.debug("restarting emulator")
    kill_emulator(name=avd_name, uuid=uuid)
    time.sleep(5)
    LOGGER.debug("emulator shutdown")
    start_emulator(name=avd_name)
    LOGGER.debug("emulator restarted")
    return dumps({"stdout": 'success', "stderr": ''})


if __name__ == '__main__':
    if not check_sudo():
        LOGGER.error("Must run script as root")
        exit(1)
    try:
        avd_port = int(os.environ['AVD_PORT'])
    except KeyError:
        avd_port = 15001
    try:
        debug = True if os.environ['DEBUG'] else False
        if debug:
            app.logger.setLevel(logging.DEBUG)
    except KeyError:
        debug = False
    app.run(debug=debug, host='0.0.0.0', port=avd_port)
