export AVD_IP=$(ifconfig eno1 | egrep -o "inet addr:(([0-9]{1,3}\.){3}[0-9]{1,3})" | cut -d ":" -f 2)
export AVD_PORT=15001
export CMDR_IP=localhost
export CMDR_PORT=6000
export DRIVER_IFACE=eno1
export DROID_USER_HOME=/home/lthurlow
export DROID_EMU_PATH=/home/lthurlow/droid-emu/

# debug for avd_host
export DEBUG=True
