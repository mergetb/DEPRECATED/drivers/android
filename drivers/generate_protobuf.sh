#!/bin/bash

python -m grpc_tools.protoc -I ../../../api --python_out=./ --grpc_python_out=./ ../../../api/driver.proto ../../../api/shared.proto ../../../api/commander.proto
