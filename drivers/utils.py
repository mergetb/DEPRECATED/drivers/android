import logging  # for logging/debugging
import uuid as glob_uuid  # generate a uuid for the device for this driver
import json  # needed for options fields
import os.path  # check if a directory exists
import socket  # check port is open to use
from fcntl import ioctl  # get ip from socket
from struct import Struct, pack  # get ip from socket

from typing import Dict, Union  # python typing

logging.basicConfig(level=logging.DEBUG)

DRIVER_FILE = '.driver-conections.log'


class UnRegisteredUUID(Exception):
    pass


class UnhealthyCommander(Exception):
    pass


class DriverError(Exception):
    pass


# from stackoverflow on getting ip address from iterface
def get_ip_address(ifname: str) -> str:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    stru = Struct('256s')
    return socket.inet_ntoa(
        ioctl(
            sock.fileno(),
            0x8915,  # SIOCGIFADDR
            stru.pack(ifname[:15].encode('utf-8'))
        )[20:24]
    )


# appearently uuid has an rfc stipulating 128 bits
def get_uuid_for_device() -> str:
    return str(glob_uuid.uuid4()).replace('-', '')


# Find an open port which can be used to communicate with the commander
def get_open_port(port_start: int = 8000) -> int:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port_end = port_start * 2 if port_start * 2 < 65000 else 65000
    for port_val in range(port_start, port_end):
        result = sock.connect_ex(('localhost', port_val))
        sock.close()
        # if fails to connect, select that port, error value is 9
        if result == 9:
            return port_val
    return 0


# Open the connection log and find the device info associated with uuid
def read_attr_device_log(uuid: str) -> Union[None, Dict]:
    devices = []
    if not os.path.exists(DRIVER_FILE):
        with open(DRIVER_FILE, 'w') as d_log:
            d_log.write('[]')
    with open(DRIVER_FILE, 'r') as d_log:
        devices = json.loads(d_log.read())
    for device in devices:
        if device.get('uuid', None) == uuid:
            return device
    return None


# Update a log entry for device, usually overwriting in-place dict for uuid
def update_device_log(new_device: Dict) -> None:
    # write the container / uuid to a file
    connections = None
    # create if it doesnt exist
    if not os.path.exists(DRIVER_FILE):
        with open(DRIVER_FILE, 'w')as d_log:
            d_log.write('[]')
    # load into connections
    with open(DRIVER_FILE, 'r') as d_log:
        connections = json.loads(d_log.read())
    # go through list of dicts, if uuid present, delete to add new device info
    uuid_id = new_device['uuid']
    for device in connections:
        if uuid_id in device.get('uuid', None):
            connections.remove(device)
            break
    # now add the new device in.
    connections.append(new_device)
    # write back our update to log file
    with open(DRIVER_FILE, 'w') as d_log:
        d_log.write(json.dumps(connections))


# Remove uuid from device log
def remove_device_from_log(uuid: str) -> None:
    # if file doesnt exist, what are we doing
    if not os.path.exists(DRIVER_FILE):
        raise UnRegisteredUUID
    # load into connections
    connections = []
    with open(DRIVER_FILE, 'r') as d_log:
        connections = json.loads(d_log.read())
    index = -1
    for driver in connections:
        if driver['uuid'] == uuid:
            index = connections.index(driver)
    if index == -1:
        raise UnRegisteredUUID
    _ = connections.pop(index)
    # write back our update to log file
    with open(DRIVER_FILE, 'w') as d_log:
        d_log.write(json.dumps(connections))
