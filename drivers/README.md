# Android Driver Implementation

### Building

To build the driver implementation, first `pip install -r requirements.txt`.  It is required that the minimum version is python 3.6 in order to support python typing notation (`:` and `->`).

Next you will need to compile the protobuf definitions located in the `site/api` repo.  `generate_protobuf.sh` will create local copies of the generated pb2 code in the local directory.  This is useful for local testing.

### Running

After building the python 3.6 requirements, you can run the code using `python drivers.py`.  You can also, build it using the docker compose file in the parent directory, however it is not intended to be used other than with testing.  You would otherwise need to run `docker-compose build` and `docker-compose up` in the parent directory.

### Files in this directory.

```
droid_utils.py: python utility functions for communicating with android flask api
Dockerfile: used to generate a docker image which can be used to run this code (sans protobuf defn)
generate_protobuf.sh: run python's protobuf compiler to generate pb2 code that can be imported
requirements.txt: python requirements, generated using pip freeze -l > requirements.txt
run_driver_server.sh: bash script to run the code as a server, used by docker-compose file
setup.cfg: pylint/flake8, python styling guidelines
test_drivers.py: python unittesting code to verify the driver code works
test_utils.py: python unittesting code to verify the various utility functions work
utils.py: python utility functions for handling generic functionality
drivers.py: main python code that implements the driver state machine
```

#### Requests by Commander

Below are some examples of json examples of what can be parsed by the android driver.

##### On Request

Currently the android driver has only been tested with version 8.0 on pixel devices.  There is nothing that limits those values, so you could change the device, and version, but at the moment those values are hardcoded.  sdcard is a valid key, but at the moment should not be used.

```
device: "pixel"
sdcard:
sdk_api: "P"
abi: "x86_64"
name: "device_1"
```

#### Off Request

There is no json that is used for the TurnOff request.

#### Setup Request

There is no json that is used for the Setup request.  In the future, this request will be used, however with the virtualized Android device, all of the setup json is passed in for OnRequests.

#### Configure Request

The configuration request only accepts json fields: `apks`, `push`, `pull`, and `shell`. `apks` and `shell` take in a list.  `push` and `pull` take in a list of lists, where the inner lists are really a tuple for local and remote files between the local file system and the android filesystem.

```
// install local apks to the android device
apks: []
// start an appium instance to connect to the node
appium: True/False
// copy files from host to devices
push: [['local', 'remote'], ['local', 'remote']]
// copy files to host from device
pull: [['remote', 'local'], ['remote', 'local']]
// command execution on android, https://developer.android.com/studio/command-line/adb.html#shellcommands
shell: ['pm list packages', 'am broadcast "intent:#Intent;action=android.intent.action.BATTERY_CHANGED;i.status=5;i.voltage=4155;i.level=100;end"', 'ls']
```

#### Restart Request

There is no json that is used for the Restart request.

#### HealthCheck Request

There is no json that is used for the HealthCheck request.
