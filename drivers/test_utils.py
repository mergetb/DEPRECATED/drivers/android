import unittest
import logging
import os

from utils import (
    update_device_log,
    read_attr_device_log,
    remove_device_from_log,
    DRIVER_FILE,
)

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


# pylint: disable=invalid-name
class UtilTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print('setup')
        super(UtilTests, cls).setUpClass()
        if os.path.exists(DRIVER_FILE):
            os.remove(DRIVER_FILE)

    @classmethod
    def tearDownClass(cls):
        print('teardown')
        super(UtilTests, cls).tearDownClass()
        if os.path.exists(DRIVER_FILE):
            os.remove(DRIVER_FILE)

    def test_creating_log_file(self):
        blob = {
            'os': 'android',
            'os_version': '8',
            'phy_device': 'nexus',
            'uuid': '1',
            'pid': '1',
            'flask_ip': 'localhost',
            'flask_port': '5001',
            'adb_ip': '1.1.1.1',
            'adb_port': '12',
        }
        update_device_log(blob)
        blob['uuid'] = '2'
        blob['pid'] = '2'
        blob['flask_port'] = '5002'
        update_device_log(blob)
        blob['uuid'] = '3'
        blob['pid'] = '3'
        blob['flask_port'] = '5003'
        blob['phy_device'] = 'pixel'
        update_device_log(blob)

        # now check values
        b1 = read_attr_device_log('1')
        b2 = read_attr_device_log('2')
        b3 = read_attr_device_log('3')
        LOGGER.debug(str(b1))
        LOGGER.debug(str(b2))
        LOGGER.debug(str(b3))

        self.assertTrue(b1['pid'] == '1')
        self.assertTrue(b2['pid'] == '2')
        self.assertTrue(b3['pid'] == '3')

    def test_deleting_log_file(self):
        remove_device_from_log('1')
        remove_device_from_log('3')

        b1 = read_attr_device_log('1')
        b2 = read_attr_device_log('2')
        b3 = read_attr_device_log('3')

        LOGGER.debug(str(b1))
        LOGGER.debug(str(b2))
        LOGGER.debug(str(b3))

        self.assertIsNone(b1)
        self.assertIsNotNone(b2)
        self.assertIsNone(b3)


if __name__ == '__main__':
    unittest.main()
