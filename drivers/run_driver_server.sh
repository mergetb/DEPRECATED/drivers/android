#!/bin/bash

python -m grpc_tools.protoc -I /protobuf --python_out=./ --grpc_python_out=./ /protobuf/driver.proto /protobuf/shared.proto /protobuf/commander.proto
python /drivers/drivers.py
