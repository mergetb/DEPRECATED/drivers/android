import logging  # for logging/debugging
from subprocess import PIPE, Popen  # handle all communication with android
from typing import Dict, List  # python type checking
from time import sleep  # use for polling if sdcard is mounted

from utils import read_attr_device_log as read_log  # get device attributes

# pylint: disable=fixme

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

ADB_EXE = "./adb"  # docker container needs path to adb, this is git-commited binary


class DeviceNotRegistered(Exception):
    pass


# We need a means of getting the uuids for each device - in order to do so, each device is tagged
# with a uuid and name in /system/, to read or write, the adb connection requires root access
# which requires getting root and remounting the system filesystem.
def get_device_uuids() -> List[Dict]:
    proc = Popen([ADB_EXE, 'devices', '-l'], stdout=PIPE, stderr=PIPE)
    out, _ = proc.communicate()
    # List, x1, x2, x3 - [1:] remove List from the list, [0] is serial number
    serials = [x.split(' ')[0] for x in out.decode().strip().split('\n')][1:]
    serial_uuid = []
    LOGGER.info(serials)
    for droid in serials:
        LOGGER.info(droid)
        uuid = ""
        proc = Popen([ADB_EXE, '-s', droid, 'root'], stdout=PIPE, stderr=PIPE)
        _, err = proc.communicate()
        if err:
            LOGGER.error("error rooting adb for %s: %s", droid, str(err))
            continue  # if we cant get root, no use running any other commands
        proc = Popen([ADB_EXE, '-s', droid, 'remount'], stdout=PIPE, stderr=PIPE)
        _, err = proc.communicate()
        if err:
            LOGGER.error("error remounting system for %s: %s", droid, str(err))
            continue  # if we cant remount, no use trying to read files
        proc = Popen(
            [ADB_EXE, '-s', droid, 'shell', 'cat', '/system/uuid'],
            stdout=PIPE, stderr=PIPE,
        )
        out, err = proc.communicate()
        # if there was an error reading the uuid, it may not exist.
        # the device should be rebuilt using the driver state machine.
        if err:
            LOGGER.error("error checking uuid for %s: %s", droid, str(err))
            continue  # this device cannot be tracked by us, may be some other processes device

        proc = Popen([ADB_EXE, '-s', droid, 'unroot'], stdout=PIPE, stderr=PIPE)
        _, err = proc.communicate()
        if err:
            LOGGER.error("error unrooting adb: %s", str(err))
        serial_uuid.append({uuid: droid})
    # FIXME: bad code
    return [list(x.keys())[0] for x in serial_uuid]


# install android apk
def install_apk(uuid: str, pkg_path: str) -> Dict:
    packages = [pkg_path]
    options = []  # TODO: Implement
    serial = read_log(uuid=uuid).get('serial', None)
    if not serial:
        return {"stdout": "", "stderr": "unable to find serial connected to {}".format(uuid)}
    str_options = ['-' + str(x) for x in options]
    if len(packages) == 1:
        proc = Popen(
            [ADB_EXE, '-s', serial, 'install', str_options, '{}'.format(packages[0])],
            stdout=PIPE, stderr=PIPE
        )
        out, err = proc.communicate()
        return {"stdout": out.decode('utf-8').strip(), "stderr": err.decode('utf-8').strip()}
    proc = Popen(
        [ADB_EXE, '-s', serial, 'install-multiple', '%s' if str_options else '', packages],
        stdout=PIPE, stderr=PIPE
    )
    out, err = proc.communicate()
    return {"stdout": out.decode('utf-8').strip(), "stderr": err.decode('utf-8').strip()}


# TODO: for when there is ceph cluster
def get_remote_file(local: str) -> str:
    return local


# put file from local machine (localhost) onto android
# TODO: allow user to write in protected areas - requires re-thinking /system/uuid method
def push(uuid: str, local: str, remote: str) -> bool:
    serial = read_log(uuid=uuid).get('serial', None)
    if not serial:
        return {"stdout": "", "stderr": "unable to find serial connected to {}".format(uuid)}
    if not local or not remote:
        return {"stdout": "", "stderr": "'local' or 'remote' was not specified - but required"}
    tmp_file = get_remote_file(local)
    push_proc = Popen(
        [ADB_EXE, '-s', serial, 'push', tmp_file, remote],
        stdout=PIPE, stderr=PIPE
    )
    out, err = push_proc.communicate()
    return {"stdout": out.decode('utf-8').strip(), "stderr": err.decode('utf-8').strip()}


# TODO: ceph
def put_remote_file(src: str) -> str:
    return src


# get file from android device onto local machine (localhost)
def pull(uuid: str, remote: str, local: str) -> bool:
    serial = read_log(uuid=uuid).get('serial', None)
    if not serial:
        return {"stdout": "", "stderr": "unable to find serial connected to {}".format(uuid)}
    if not local or not remote:
        return {"stdout": "", "stderr": "'local' or 'remote' was not specified - but required"}
    tmp_file = put_remote_file(local)
    pull_proc = Popen(
        [ADB_EXE, '-s', serial, 'pull', remote, tmp_file],
        stdout=PIPE, stderr=PIPE
    )
    out, err = pull_proc.communicate()
    return {"stdout": out.decode('utf-8').strip(), "stderr": err.decode('utf-8').strip()}


# execute a shell command on the android device
# TODO: allow the shell command to be run as root
def run_shell(uuid: str, shell_cmd: str) -> Dict:
    serial = read_log(uuid=uuid).get('serial', None)
    LOGGER.debug('run_shell: %s, serial: %s', shell_cmd, serial)
    if not serial:
        return {"stdout": "", "stderr": "unable to find serial connected to {}".format(uuid)}
    shell_proc = Popen(
        [ADB_EXE, '-s', serial, 'shell', shell_cmd],
        stdout=PIPE, stderr=PIPE
    )
    out, err = shell_proc.communicate()
    LOGGER.debug("%s\n%s\n", out.decode(), err.decode())
    return {"stdout": out.decode('utf-8').strip(), "stderr": err.decode('utf-8').strip()}


# if the sdcard is not mounted, lets assume the system
# is not fully booted, we can use this instead of time
# waits to enforce the system is ready to begin.
# return true if we load sdcard before timeout
def wait_on_sdcard(uuid: str, timeout=60) -> bool:
    cmd = run_shell(uuid, "ls /sdcard")
    count = 0
    while cmd['stderr'] and count < timeout:
        sleep(2)
        cmd = run_shell(uuid, "ls /sdcard")
        count += 2
    if cmd['stderr']:
        return False
    return True
