import logging  # for logging/debugging
import json  # needed for options fields
import time  # for sleeping server
import os  # used for getting environment variables
from concurrent import futures  # backwards compatable threads?
from typing import Any, Dict, List, Union  # python type checking
import requests  # talk to AVD manager

import grpc
import shared_pb2
import driver_pb2_grpc
import commander_pb2
import commander_pb2_grpc

# these are the functions that implement device tracking and communication
from utils import (
    DriverError,
    UnhealthyCommander,
    get_open_port,
    get_ip_address,
    read_attr_device_log,
    update_device_log,
)

# these are the functions that implement the base functionality for setup/configure
from droid_utils import (
    get_device_uuids,
    install_apk,
    push,
    pull,
    run_shell,
    wait_on_sdcard,
)

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

# Disable non-member, required for protobuf generated code
# pylint: disable=no-member

# pylint: disable=fixme
# NOTE: all driver code should be blocking

# Defer the knoweledge of where the commander is located to an environment variable
COM_CHANNEL = grpc.insecure_channel('%s:%s' % (os.environ['CMDR_IP'], os.environ['CMDR_PORT']))
COM_STUB = commander_pb2_grpc.CommanderStub(COM_CHANNEL)

# Enforce the api and devices enabled for machine, additions require sdk manager installs
VERSIONS = ['26', 'P']
DEVICES = ['pixel']


# check if the uuid is a valid uuid currently tracked by driver
def check_invalid_uuid(response: Union[shared_pb2.OnResponse, shared_pb2.OffResponse],
                       dev_uuid: str) -> Union[shared_pb2.OnResponse, shared_pb2.OffResponse, None]:
    device_info = read_attr_device_log(dev_uuid)
    LOGGER.debug("Data (%s): %s", dev_uuid[:16], str(device_info))
    # if the uuid is not in the database, we should ignore, if this is a physical
    # device, it is not in the database
    if not device_info:
        diag = response.response.msg.add()
        diag.code = 3
        response.response.return_code = 1
        diag.msg = "uuid not found: {}".format(device_info)
        return response
    return None


# Execute a callback to commander to verify the commander is healthy
def commander_health_check() -> int:
    health_check_resp = COM_STUB.HealthCheck(shared_pb2.HealthCheckRequest())
    if health_check_resp.status != 0:
        LOGGER.error("commander: health check failed.")
        return 1
    LOGGER.debug("commander: health check succeeded.")
    return 0


# Unregister an android device with the commander
def unregister_with_commander(uuids: List[str], testing: bool = False) -> bool:
    unregister_request = commander_pb2.UnregisterRequest()
    if not testing:
        unregister_request.uuid.extend(uuids)
    else:
        dev_uuid = '43e953281da511e8'
        unregister_request.uuid.append(dev_uuid)
    try:
        response = COM_STUB.Unregister(unregister_request)
        LOGGER.info(response)
    # pylint: disable=protected-access
    except grpc._channel._Rendezvous:
        LOGGER.error("cmdr offline, testing driver server")
        raise UnhealthyCommander("cmdr offline")


# register with commander, True if okay, raise error otherwise
def register_with_commander(port: int) -> bool:
    try:
        ip_addr = os.environ['DRIVER_IP']
    except KeyError:
        ip_addr = get_ip_address(os.environ['DRIVER_IFACE'])
    if not ip_addr:
        LOGGER.error("driver does not have ip address set for DRIVER_IP or DRIVER_IFACE")
        raise UnhealthyCommander("Cannot communicate with Commander without setting DRIVER_IFACE")
    # need to have server up and running so commander can healthcheck
    register_request = commander_pb2.RegisterRequest()
    register_request.host = get_ip_address(os.environ['DRIVER_IFACE'])
    register_request.port = port
    register_request.driver = 'android'
    # this will register all resources to ceftb that have been correctly tagged
    register_request.uuid.extend(get_device_uuids())
    try:
        response = COM_STUB.Register(register_request)
        LOGGER.info(response)
        return True
    # pylint: disable=protected-access
    except grpc._channel._Rendezvous:
        LOGGER.info("cmdr offline, testing driver server")
        raise UnhealthyCommander("cmdr offline")


# most of the time the diagnostics will be filled based on stdout and stderr
def autofill_diag(resp: Union[shared_pb2.SetupResponse, shared_pb2.ConfigureResponse,
                              shared_pb2.OnResponse, shared_pb2.OffResponse],
                  output: Dict[str, str]) -> None:
    diag = resp.response.msg.add()
    if output['stderr']:
        diag.code = 3
        diag.msg = output['stderr']
    else:
        diag.code = 1
        diag.msg = output['stdout']


# Go through all the diagnosis messages, if there was an error, return error
def set_response_code(resp) -> int:
    for responses in resp.response.msg:
        if responses.code > 1:
            return 1
    return 0


def handle_ordered_execution(ordered_cmds: List[Dict[str, Any]], uuid: str) -> List[Dict[str, str]]:
    output = []
    for cmd in ordered_cmds:
        if isinstance(cmd, dict):
            cmd_key = cmd.keys()[0]
            if cmd_key == "apks":
                out = install_apk(uuid, cmd[cmd_key])
            elif cmd_key == "push":
                local, remote = cmd[cmd_key]
                out = push(uuid, local, remote)
            elif cmd_key == "pull":
                local, remote = cmd[cmd_key]
                out = pull(uuid, remote, local)
            elif cmd_key == "shell":
                out = run_shell(uuid, cmd[cmd_key])
            if out:
                output.append(out)
    return output


# called by configure and setup to implement certain functionality fo the droid
def droid_settings(dev_uuid, response, input_data) -> \
        Union[shared_pb2.SetupResponse, shared_pb2.ConfigureResponse]:
    # parse all the inputs
    config = json.loads(input_data)
    apks = config.get('apks', [])
    fi_copy_host = config.get('pull', [])
    fi_copy_droid = config.get('push', [])
    shell_cmds = config.get('shell', [])
    ordered_cmds = config.get('ordered', [])
    LOGGER.debug(config)
    outputs = []
    # TODO: very basic type checking, in future requires sanitization of inputs
    if ordered_cmds:
        outputs = handle_ordered_execution(ordered_cmds, dev_uuid)
    else:
        for apk in apks:
            if isinstance(apk, str):
                outputs.append(install_apk(dev_uuid, apk))
        for paths in fi_copy_droid:
            if isinstance(paths, list):
                outputs.append(push(dev_uuid, paths[0], paths[1]))
        for paths in fi_copy_host:
            if isinstance(paths, list):
                outputs.append(pull(dev_uuid, paths[1], paths[0]))
        for cmd in shell_cmds:
            if isinstance(cmd, str):
                outputs.append(run_shell(dev_uuid, cmd))
    for out in outputs:
        autofill_diag(response, out)
    response.response.return_code = set_response_code(response)
    return response


# implements all of the driver functionality
class DeviceServicer(driver_pb2_grpc.DeviceServicer):
    def __init__(self):
        self.avd_host = os.environ['AVD_IP']
        self.avd_port = os.environ['AVD_PORT']
        self.conn_str = "http://{host}:{port}".format(host=self.avd_host, port=self.avd_port)
        # get the avd server information to communicate with avd/emulator
        LOGGER.debug("AVD SETTINGS: %s:%s", self.avd_host, self.avd_port)
        if not self.avd_host or not self.avd_port:
            raise DriverError(
                "AVD_IP ({avd_host}) or AVD_PORT ({avd_port}) was not set".format(
                    avd_host=self.avd_host,
                    avd_port=self.avd_port
                )
            )

    def HealthCheck(self, request, context) -> shared_pb2.HealthCheckResponse:
        LOGGER.info("HEALTH: docter check up: %s", request)
        hc_response = shared_pb2.HealthCheckResponse()
        hc_response.status = 0
        return hc_response

    def NotifyIncoming(self, request, context) -> shared_pb2.NotifyIncomingResponse:
        LOGGER.info("INCOMING: docter check up: %s", request)
        ic_response = shared_pb2.IncomingResponse()
        ic_response.accept = True
        return ic_response

    def Configure(self, request, context) -> shared_pb2.ConfigureResponse:
        dev_uuid = request.deviceId
        LOGGER.info("CONF RECV: %s, %s", dev_uuid[:16], request.data)
        configure_response = shared_pb2.ConfigureResponse()
        configure_response.response.deviceId = dev_uuid
        ret_resp = droid_settings(dev_uuid, configure_response, request.data)
        return ret_resp

    def Setup(self, request, context) -> shared_pb2.SetupResponse:
        dev_uuid = request.deviceId
        LOGGER.debug("SETUP Recv: %s", dev_uuid[:16])
        setup_response = shared_pb2.SetupResponse()
        setup_response.response.deviceId = dev_uuid
        ret_resp = droid_settings(dev_uuid, setup_response, request.data)
        return ret_resp

    def TurnOff(self, request, context) -> shared_pb2.OffResponse:
        dev_uuid = request.deviceId
        LOGGER.debug("OFF Recv: %s", dev_uuid[:16])
        response = shared_pb2.OffResponse()
        response.response.deviceId = dev_uuid
        # look into our device log for a record of the device
        invalid = check_invalid_uuid(response, dev_uuid)
        if invalid:
            return invalid
        # check if the emulator is running
        req = requests.post(self.conn_str + "/emulator/ps", json={"name": dev_uuid})
        LOGGER.debug(req.text)
        # if we dont get back a value, ps and therefore name not found in ps table
        if not req.text:
            autofill_diag(response, {"stdout": "", "stderr": "Device is already Off."})
        # if we got a response, we need to kill the associate ps from req.text
        else:
            req = requests.post(
                '{conn}/emulator/kill'.format(conn=self.conn_str),
                json={'uuid': dev_uuid},
            )
            LOGGER.debug(req.json())
            autofill_diag(response, req.json())
        update_device_log({'uuid': dev_uuid})
        return response

    def TurnOn(self, request, context) -> shared_pb2.OnResponse:
        dev_uuid = request.deviceId
        LOGGER.debug("ON Recv: %s, data:%s", dev_uuid[:16], request.data)
        response = shared_pb2.OnResponse()
        response.response.deviceId = dev_uuid
        # look into our device log for a record of the device
        invalid = check_invalid_uuid(response, dev_uuid)
        if invalid:
            return invalid
        device_info = dev_uuid
        # use setup_json to send data to emulator
        setup_json = json.loads(request.data)
        setup_json['name'] = device_info
        setup_json['uuid'] = device_info

        # check if emulator is already running.
        req = requests.post(self.conn_str + "/emulator/ps", json={"name": device_info})
        # if we get back a response, our emulator is already running and we return.
        if req.text:
            LOGGER.info("Device already on. Resetting State.")
            LOGGER.info(str(request.data))
            reset = requests.post(self.conn_str + "/emulator/reset", json=setup_json)
            reset_res = reset.json()
            autofill_diag(response, reset_res)
            req = requests.get(
                '{conn}/emulator/serial'.format(conn=self.conn_str),
                json={'uuid': device_info}
            )
            LOGGER.debug('serial device- %s : %s', device_info, req.text)
            # if the response is not none we have a serial device to add
            update_device_log({'uuid': device_info, 'serial': req.text})
            _ = wait_on_sdcard(device_info)
            return response
        # if the response is empty, the device is not running, we need to start it
        else:
            # first we need to check for the avd, if it doesnt exist, then we are
            # emulator, and TODO fix it so that the avd must exist prior, but this
            # seems like a more scalable approach to handling emulated devices on On.
            avd = requests.post(self.conn_str + "/avd/check", json={"name": device_info})
            LOGGER.debug("Checking AVD image exists: %s", str(avd.json()))
            exists = avd.json()['stdout'] == 'success'
            if not exists:
                # create the avd image name using the uuid for tracking
                # TODO: validate
                req = requests.post(
                    '{conn}/avd/create'.format(conn=self.conn_str),
                    json=setup_json,
                )
                # the requests return code wont tell us of error, check the stderr of dict
                autofill_diag(response, req.json())
                return response
            # now we actually get to turning on the emulator for the device
            # TODO: sanitize
            req = requests.post(
                '{conn}/emulator/start'.format(conn=self.conn_str),
                json=setup_json,
            )
            autofill_diag(response, req.json())

            LOGGER.debug("Emulator started - tagging emulated device")
            # everytime we start the emulator, lets make sure the device is tagged.
            req = requests.post(
                '{conn}/emulator/tag'.format(conn=self.conn_str),
                json={'name': device_info, 'uuid': device_info, 'first_boot': 'True'}
            )
            LOGGER.debug(req.json())
            autofill_diag(response, req.json())
            # we are also going to tag the serial interface for other easy application use
            req = requests.get(
                '{conn}/emulator/serial'.format(conn=self.conn_str),
                json={'uuid': device_info}
            )
            LOGGER.debug('serial device- %s : %s', device_info, req.text)
            # if the response is not none we have a serial device to add
            if req.text:
                # update our driver log entry for uuid to include serial
                update_device_log({'uuid': device_info, 'serial': req.text})
                # wait for the sdcard to come online before returning
                _ = wait_on_sdcard(device_info)
            else:
                autofill_diag(
                    response,
                    {"stdout": "", "stderr": "unable to find serial connection for uuid: {}".format(
                        device_info
                    )}
                )
            return response

    def Restart(self, request, context) -> shared_pb2.RestartResponse:
        dev_uuid = request.deviceId
        LOGGER.info("RESTART Recv: %s", dev_uuid[:16])
        restart_response = shared_pb2.RestartResponse()
        restart_response.response.deviceId = dev_uuid
        LOGGER.info("Restarting Device")
        restart = requests.post(self.conn_str + "/emulator/restart", json={'uuid': dev_uuid})
        restart_res = restart.json()
        autofill_diag(restart_response, restart_res)
        # wait for the sdcard to come online before returning
        _ = wait_on_sdcard(dev_uuid)
        return restart_response

    def Reset(self, request, context) -> shared_pb2.ResetResponse:
        dev_uuid = request.deviceId
        LOGGER.info("RESET Recv: %s", dev_uuid[:16])
        reset_response = shared_pb2.ResetResponse()
        reset_response.response.deviceId = dev_uuid
        LOGGER.info("Resetting Device")
        reset = requests.post(self.conn_str + "/emulator/reset", json=json.loads(request.data))
        reset_res = reset.json()
        autofill_diag(reset_response, reset_res)
        return reset_response


def run_device_server(testing: bool = False, client_testing: bool = False):
    # FIXME: need to check that between finding port, registering
    # it and starting server, that nothing swoops in to take it.
    if testing or client_testing:
        open_port = 8001
        # wipe out driver logs for testing
        with open('.driver-conections.log', 'w') as dlog:
            dlog.write('[]')
        if client_testing:
            update_device_log({'uuid': '43e953281da511e8'})
    else:
        open_port = get_open_port(8000)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    driver_pb2_grpc.add_DeviceServicer_to_server(DeviceServicer(), server)
    # TODO: make the port random in range
    server.add_insecure_port('localhost:%s' % str(open_port))
    server.start()
    # register with the commander
    if not client_testing:
        _ = register_with_commander(open_port)
    try:
        while True:
            # server is non-blocking, so sleep for 1 day // google recommended
            time.sleep(time.sleep(86400))
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    run_device_server(testing=False, client_testing=False)
