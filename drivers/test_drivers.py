# from the github page on appium
import unittest
import os
import logging
import json

import driver_pb2

from drivers import DeviceServicer
from utils import UnRegisteredUUID

# pylint: disable=no-member

DRIVER_FILE = '.driver-conections.log'
MAPPIN_FILE = '.devices.map'
# LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class AndroidDriverTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setup')
        super(AndroidDriverTests, cls).setUpClass()
        if os.path.exists(DRIVER_FILE):
            os.remove(DRIVER_FILE)
        cls.uuid = '5f73e582181d11e8a9830cc47ad891668e4e3445d0d64185898e33d323599a8b'
        serial = 'emulator-5558'
        with open(MAPPIN_FILE, 'w') as mapping:
            mapping.write(json.dumps([{cls.uuid: serial}]))
        cls.test = DeviceServicer()

    @classmethod
    def tearDownClass(cls):
        print('teardown')
        super(AndroidDriverTests, cls).tearDownClass()
        if os.path.exists(DRIVER_FILE):
            os.remove(DRIVER_FILE)

    # unittest does alphabetical ordering.

    # turn on and initialize device
    def test_a_on(self):
        request = driver_pb2.TurnOnRequest()
        request.device.uuid = self.uuid
        request.data.json = '{"operating_system":"android", "version": "8.0.0", '\
            ' "device": "pixel"}'
        self.test.TurnOn(request, None)

    # test setting configuration settings via shell
    def test_b_configure(self):
        request = driver_pb2.ConfigureRequest()
        request.device.uuid = self.uuid
        request.data.json = '{"shell": ["ls -la"]}'
        self.test.Configure(request, None)

    # test reboot/restart
    def test_c_reboot(self):
        request = driver_pb2.RestartDeviceRequest()
        request.device.uuid = self.uuid
        self.test.Restart(request, None)

    # test that we can still configure after reboot
    def test_d_configure(self):
        request = driver_pb2.ConfigureRequest()
        request.device.uuid = self.uuid
        request.data.json = '{"shell": ["ls -la /sdcard/"]}'
        self.test.Configure(request, None)

    # test turning off/cleaning state
    def test_e_off(self):
        request = driver_pb2.TurnOffRequest()
        request.device.uuid = self.uuid
        self.test.TurnOff(request, None)

    # test turning off an already off/de-allocated machine
    def test_f_off(self):
        request = driver_pb2.TurnOffRequest()
        request.device.uuid = self.uuid
        with self.assertRaises(UnRegisteredUUID):
            self.test.TurnOff(request, None)

    # test turning on the machine from off state again
    def test_g_on(self):
        request = driver_pb2.TurnOnRequest()
        request.device.uuid = self.uuid
        request.data.json = '{"operating_system":"android", "version": "8.0.0", '\
            ' "device": "pixel"}'
        self.test.TurnOn(request, None)

    # test on when device is already on
    def test_h_on(self):
        request = driver_pb2.TurnOnRequest()
        request.device.uuid = self.uuid
        request.data.json = '{"operating_system":"android", "version": "8.0.0", '\
            ' "device": "pixel"}'
        self.test.TurnOn(request, None)

    # make sure everything is off for next test
    def test_i_off(self):
        request = driver_pb2.TurnOffRequest()
        request.device.uuid = self.uuid
        with self.assertRaises(UnRegisteredUUID):
            self.test.TurnOff(request, None)


if __name__ == '__main__':
    unittest.main()
